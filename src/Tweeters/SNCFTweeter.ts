import Twit from "twit";
import { Configuration } from "../Configuration";
import { SNCFFormatter } from "../Formatters/SNCFFormatter";
import { Constants } from "../Utils/Constants";
import { Logger } from "../Utils/Logger";

const logger = Logger.getLogger();

export class SNCFTweeter {
  private _formatter: SNCFFormatter;
  private _twit: Twit;

  constructor(formatter: SNCFFormatter) {
    this._formatter = formatter;
    this._twit = new Twit({
      access_token: Configuration.configuration.twitterAccessToken,
      access_token_secret: Configuration.configuration.twitterAccessTokenSecret,
      consumer_key: Configuration.configuration.twitterConsumerKey,
      consumer_secret: Configuration.configuration.twitterConsumerSecret,
      strictSSL: true,
      timeout_ms: 60 * 1000,
    });
  }

  public async tweet() {
    logger.info("Posting thread...");
    await this.postThread([
      this._formatter.formatDisruptions(),
      this._formatter.formatDisruptionCauses(),
      this._formatter.formatDisruptionStops(),
    ]);
  }

  private async postThread(statuses: string[]): Promise<Twit.PromiseResponse[]> {
    const responses: Twit.PromiseResponse[] = [];

    // Check if any of the Tweets will exceed the maximum Tweet length, just in case.
    if (statuses.some((status) => status.length > Constants.TWITTER_MAX_TWEET_LENGTH)) {
      throw new Error(
        `Some Tweets from the thread exceed the maximum Tweet length (${Constants.TWITTER_MAX_TWEET_LENGTH}).`,
      );
    }

    for (let status of statuses) {
      const lastResponse = responses[responses.length - 1];
      let replyToStatusId: string | undefined;

      if (lastResponse !== undefined) {
        // If this isn't the first tweet, get the username and the ID from the previous Tweet.
        const username = (lastResponse.data as any).user.screen_name;
        replyToStatusId = (lastResponse.data as any).id_str;
        // Prepend the username to the next Tweet in the thread.
        status = `@${username} ${status}`;
      }

      responses.push(await this.postStatus(status, replyToStatusId));
    }

    return responses;
  }

  private async postStatus(status: string, replyToStatusId?: string): Promise<Twit.PromiseResponse> {
    return await this._twit.post("statuses/update", {
      in_reply_to_status_id: replyToStatusId,
      status,
    } as Twit.Params);
  }
}
