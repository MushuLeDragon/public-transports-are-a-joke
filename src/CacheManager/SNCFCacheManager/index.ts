import { readFile, writeFile } from "fs";
import moment from "moment";
import path from "path";
import { CacheManager } from "..";
import { Logger } from "../../Utils/Logger";
import { MomentHelper } from "../../Utils/MomentHelper";
import { ISNCFCacheData } from "./ISNCFCacheData";
import makeDir = require("make-dir");

const logger = Logger.getLogger();

const SNCF_CACHE_FOLDER = "sncf";

export class SNCFCacheManager {
  public static get cacheFolder(): string {
    return path.resolve(CacheManager.cacheFolder, SNCF_CACHE_FOLDER);
  }

  public static async loadFromCache(startDate: moment.Moment, endDate: moment.Moment): Promise<ISNCFCacheData> {
    if (MomentHelper.isSameDay(startDate, endDate)) {
      return this.loadSingleDayFromCache(startDate);
    } else {
      return this.loadMultipleDaysFromCache(startDate, endDate);
    }
  }

  public static async saveToCache(date: moment.Moment, data: ISNCFCacheData) {
    logger.info("Saving data to cache...");

    // Create cache folder if it doesn't already exist.
    await makeDir(this.cacheFolder);

    return new Promise<void>((resolve, reject) => {
      writeFile(this.dateToCacheFilename(date), JSON.stringify(data), "utf8", (err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }

  private static async loadSingleDayFromCache(date: moment.Moment): Promise<ISNCFCacheData> {
    logger.info(`Loading the data for a day from cache (${date})...`);

    // Create cache folder if it doesn't already exist.
    await makeDir(this.cacheFolder);

    return new Promise<ISNCFCacheData>((resolve, reject) => {
      readFile(this.dateToCacheFilename(date), "utf8", (err, data) => {
        if (err) {
          return reject(err);
        }
        const parsedData: ISNCFCacheData = JSON.parse(data);

        return resolve(parsedData);
      });
    });
  }

  private static async loadMultipleDaysFromCache(startDate: moment.Moment, endDate: moment.Moment):
  Promise<ISNCFCacheData> {
    logger.info(`Loading multiple days from cache (${startDate} to ${endDate})...`);

    const aggregatedData: ISNCFCacheData = {
      disruptionsData: [],
      totalTrips: 0,
    };

    // Iterate through each day between the start and end date (both included).
    for (const m = startDate.clone(); m.isBefore(endDate); m.add(1, "days")) {
      const cacheData = await this.loadSingleDayFromCache(m);
      aggregatedData.totalTrips += cacheData.totalTrips;
      aggregatedData.disruptionsData.push(...cacheData.disruptionsData);
    }

    return aggregatedData;
  }

  private static dateToCacheFilename(date: moment.Moment): string {
    return path.resolve(
      this.cacheFolder,
      `${MomentHelper.momentToShortDateString(date)}.json`,
    );
  }
}
