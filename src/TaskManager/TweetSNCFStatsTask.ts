import moment from "moment";
import { Configuration } from "../Configuration";
import { SNCFFormatter } from "../Formatters/SNCFFormatter";
import { SNCFProcessor } from "../Processors/SNCFProcessor";
import { SNCFTweeter } from "../Tweeters/SNCFTweeter";
import { IArgv } from "../Utils/ArgvHelper";
import { MomentHelper } from "../Utils/MomentHelper";
import { Task } from "./Task";

export enum StatsTimeSpan {
  TIME_SPAN_DAY = "day",
  TIME_SPAN_WEEK = "week",
  TIME_SPAN_MONTH = "month",
}

export class TweetSNCFStatsTask extends Task {
  private _startDate: moment.Moment;
  private _endDate: moment.Moment;

  constructor(args: IArgv) {
    super(args);

    switch (args.timeSpan) {
      case StatsTimeSpan.TIME_SPAN_DAY:
        this._startDate = MomentHelper.yesterday().startOf("day");
        this._endDate = this._startDate.clone().endOf("day");
        break;
      case StatsTimeSpan.TIME_SPAN_WEEK:
        this._startDate = moment().subtract(1, "week").startOf("week");
        this._endDate = this._startDate.clone().endOf("week");
        break;
      case StatsTimeSpan.TIME_SPAN_MONTH:
        this._startDate = moment().subtract(1, "month").startOf("month");
        this._endDate = this._startDate.clone().endOf("month");
        break;
      default:
        throw new Error(`Invalid time span '${args.timeSpan}' given for SNCF stats.`);
    }
  }

  public async execute() {
    const processor = new SNCFProcessor(this._startDate, this._endDate);
    const formatter = new SNCFFormatter(processor);

    await processor.process();

    if (Configuration.configuration.allowTweeting) {
      // In production mode, allow Tweeting.
      const tweeter = new SNCFTweeter(formatter);
      await tweeter.tweet();
    } else {
      // Only in testing mode.
      const disruptionsTweet = formatter.formatDisruptions();
      const disruptionCausesTweet = formatter.formatDisruptionCauses();
      const disruptionStopsTweet = formatter.formatDisruptionStops();

      /* tslint:disable:no-console */
      console.info(disruptionsTweet + "\n\n");
      console.info(disruptionCausesTweet + "\n\n");
      console.info(disruptionStopsTweet + "\n\n");
      /* tslint:enable:no-console */
    }
  }
}
