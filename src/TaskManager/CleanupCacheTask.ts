import fs from "fs";
import recursiveReadDir from "recursive-readdir";
import { CacheManager } from "../CacheManager";
import { Configuration } from "../Configuration";
import { IArgv } from "../Utils/ArgvHelper";
import { Logger } from "../Utils/Logger";
import { Task } from "./Task";

const logger = Logger.getLogger();

export class CleanupCacheTask extends Task {
  private _obsoleteFileThresholdDays?: number;
  private _obsoleteFileThresholdMilliseconds?: number; // In milliseconds.

  constructor(args: IArgv) {
    super(args);

    this._obsoleteFileThresholdDays = Configuration.configuration.deleteCachedFilesAfterDays;
    if (typeof this._obsoleteFileThresholdDays === "number") {
      this._obsoleteFileThresholdMilliseconds = this._obsoleteFileThresholdDays * 86_400_000;
    }
  }

  public async execute(): Promise<void> {
    if (typeof this._obsoleteFileThresholdDays === "undefined") {
      // Nothing to do.
      return;
    }

    logger.info(`Deleting cached files older than ${this._obsoleteFileThresholdDays} days...`);

    const cacheFiles = await recursiveReadDir(CacheManager.cacheFolder);
    for (const file of cacheFiles) {
      await this.deleteIfTooOld(file);
    }
  }

  private async deleteIfTooOld(filePath: string) {
    if (typeof this._obsoleteFileThresholdMilliseconds === "undefined") {
      // Nothing to do.
      return;
    }

    const obsoleteFileThresholdMilliseconds = this._obsoleteFileThresholdMilliseconds;
    return new Promise<void>((resolve, reject) => {
      fs.stat(filePath, (error, stats) => {
        if (error) {
          return reject(error);
        }
        const now = new Date().getTime();
        const endTime = new Date(stats.ctime).getTime() + obsoleteFileThresholdMilliseconds;

        // Check if file isn't obsolete.
        if (now <= endTime) {
          return resolve();
        }

        // File is obsolete, remove it.
        fs.unlink(filePath, (unlinkError) => {
          if (unlinkError) {
            return reject(error);
          }
          return resolve();
        });
      });
    });
  }
}
