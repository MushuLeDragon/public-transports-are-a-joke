import { INavitiaResponseDisruptionStop } from "../../Navitia/INavitiaResponse";
import { MomentHelper } from "../../Utils/MomentHelper";

export class DisruptionStop {
  public stop: INavitiaResponseDisruptionStop;
  public delays: number[] = []; // In seconds.
  public totalDelay: number = 0; // In seconds.
  public totalDelayedTrips: number = 0;

  constructor(stop: INavitiaResponseDisruptionStop) {
    this.stop = stop;
  }

  public addDelay(delay: number) {
    this.delays.push(delay);
    this.totalDelay += delay;
    this.totalDelayedTrips += 1;

    this.delays.sort((a, b) => b - a);
  }

  public toString(): string {
    const stats = [];
    if (this.totalDelayedTrips > 0) {
      stats.push(`${this.totalDelayedTrips} ⏰`);
    }
    if (this.totalDelay > 0) {
      stats.push(`${MomentHelper.humanizeDuration(this.totalDelay, "seconds", true)}`);
    }

    let humanizedCause = `${this.stop.label}`;
    if (stats.length > 0) {
      humanizedCause += ` (${stats.join(", ")})`;
    }

    return humanizedCause;
  }
}
