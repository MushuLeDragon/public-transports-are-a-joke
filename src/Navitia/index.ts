import requestPromise = require("request-promise");
import { Logger } from "../Utils/Logger";
import { INavitiaResponse } from "./INavitiaResponse";
import { NavitiaQuery } from "./NavitiaQuery";

const logger = Logger.getLogger();

export class Navitia {
  private _token: string;

  constructor(token: string) {
    this._token = token;
  }

  public createQuery(): NavitiaQuery {
    return new NavitiaQuery();
  }

  public async resultsCount(query: NavitiaQuery): Promise<number> {
    const results = await this.get(query.toQueryString());

    return (results as any).pagination.total_result;
  }

  public async results(query: NavitiaQuery): Promise<INavitiaResponse> {
    return this.get(query.toQueryString());
  }

  public async collection(query: NavitiaQuery, maxPages: number | null = null): Promise<any[]> {
    const collection: object[] = [];
    let nextPage: string | null = query.toQueryString();
    let pageCount = 0;

    while (nextPage && (maxPages === null || pageCount < maxPages)) {
      const results: INavitiaResponse = await this.get(nextPage);
      collection.push(...((results as any)[query.collectionName] || []));
      const nextPageObject = results.links.find((el: any) => el.type === "next");
      nextPage = nextPageObject !== undefined ? nextPageObject.href : null;
      pageCount++;
    }

    return collection;
  }

  private async get(url: string): Promise<INavitiaResponse> {
    logger.debug(`Fetching ${url}...`);
    const response: INavitiaResponse = await requestPromise({
      headers: {
        Authorization: this._token,
      },
      json: true,
      url,
    });

    if (response.error) {
      throw new Error(`Navitia API error : ${response.error.id} (${response.error.message}).`);
    }

    return response;
  }
}
