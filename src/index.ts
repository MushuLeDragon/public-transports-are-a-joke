import { Configuration } from "./Configuration";
import { TaskGroup, TaskManager } from "./TaskManager";
import { ArgvHelper } from "./Utils/ArgvHelper";
import { Logger } from "./Utils/Logger";
import { MomentHelper } from "./Utils/MomentHelper";

MomentHelper.setupMoment();

const logger = Logger.setupLogger();

async function main() {
  const argv = ArgvHelper.argv;

  // Load configuration.
  let configurationPath: string | undefined;
  if (argv.config) {
    configurationPath = argv.config as string;
  }
  await Configuration.loadConfiguration(configurationPath);

  // Execute tasks if any.
  if (argv.group) {
    await TaskManager.execute(argv.group as TaskGroup, argv);
  }
}

main()
  .catch((error) => {
    logger.error(error);
    process.exit(1);
  });
